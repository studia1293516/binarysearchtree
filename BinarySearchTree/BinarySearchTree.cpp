#include <iostream>
#include <string>
#include <stdexcept>
#include <cstdlib>

typedef struct PersonalData2 {
    std::string surname;
    std::string firstname = "John";
} PersonalDataType2;

int compare_PersonalData2(PersonalData2* person1, PersonalData2* person2) {
    int diff = person1->surname.compare(person2->surname);
    if (diff != 0) return diff;
    return person1->firstname.compare(person2->firstname);
}

int compare_PersonalData2(PersonalData2 person1, PersonalData2 person2) {
    return compare_PersonalData2(&person1, &person2);
}

int compare_int(int data1, int data2) {
    return data1 - data2;
}

std::string int_to_string(int value) {
    return std::to_string(value);
}

std::string PersonalData2ToString(PersonalData2* person) {
    return person->surname + "(" + person->firstname + ")";
}

std::string GetRandomString(int length) {
    std::string text = "";
    int charLowerSign = 97;
    int charUpperSign = 122;
    for (int i = 0; i < length; ++i) {
        text += (char)((rand() % (charUpperSign - charLowerSign + 1)) + charLowerSign);
    }
    return text;
}

PersonalData2* GetRandomPerson() {
    PersonalData2* person = new(PersonalData2);
    person->surname = GetRandomString(2);
    person->firstname = GetRandomString(1);
    return person;
}

void PrintDuration(clock_t t1, clock_t t2, std::string operation) {
    std::cout << "Operation '" << operation << "' took " << std::to_string((t2 - t1) / (double)CLOCKS_PER_SEC) << "s." << std::endl;
}

//////////////////////////////////
template <typename T> class BST {
private:
    typedef struct Item {
        T data;
        Item* left = NULL;
        Item* right = NULL;
        Item* parent = NULL;
        int id;
    };
    Item* root = NULL;
    int size = 0;
    int lastId = -1;

    std::string idToString(Item* element) {
        if (element == NULL) return "NULL";
        return std::to_string(element->id);
    }
    std::string itemsToString(Item* item, std::string separator, std::string (*dataToString)(T)) {
        return "    (" + idToString(item) + ": [p: " + idToString(item->parent) + ", l: " + idToString(item->left) + ", r: " + idToString(item->right) + "], data: " + dataToString(item->data) + ")" + separator + "\n";
    }
    void removeItem(Item* item) {
        if (item->left != NULL) removeItem(item->left);
        if (item->right != NULL) removeItem(item->right);
        delete item;
    }
    int calculateHeight(Item* item) {
        if (item == NULL)
            return 0;
        else {
            int lHeight = calculateHeight(item->left);
            int rHeight = calculateHeight(item->right);

            if (lHeight > rHeight) return lHeight + 1;
            return rHeight + 1;
        }
    }
public:
    int Size() {
        return size;
    }
    int CalcHeight() {
        return calculateHeight(root);
    }
    void AddItem(T* item, int (*data_cmp)(T, T)) {
        Item* newItem = new Item();
        newItem->data = *item;
        newItem->id = ++lastId;
        if (root == NULL) root = newItem;
        else {
            Item* temp = root;
            Item* tempParent = NULL;
            bool addLeft = false;
            while (temp != NULL) {
                tempParent = temp;
                addLeft = false;
                if (data_cmp(*item, temp->data) <= 0) {
                    temp = temp->left;
                    addLeft = true;
                }
                else temp = temp->right;
            }
            newItem->parent = tempParent;
            if (addLeft) {
                tempParent->left = newItem;
            }
            else tempParent->right = newItem;
        }
        ++size;
    }
    void AddItem(T item, int (*data_cmp)(T, T)) {
        T* tempData = new T();
        *tempData = item;
        AddItem(tempData, data_cmp);
    }

    Item* FindItem(T dataToFind, int (*data_cmp)(T, T)) {
        if (root == NULL) return NULL;
        Item* temp = root;
        while (temp != NULL && data_cmp(dataToFind, temp->data) != 0) {
            if (data_cmp(dataToFind, temp->data) < 0) {
                temp = temp->left;
            }
            else temp = temp->right;
        } 
        return temp;
    }

    void RemoveItem(Item* removeItem, int (*data_cmp)(T, T)) {
        if (removeItem != NULL) {
            if (removeItem == root && removeItem->left == NULL && removeItem->right == NULL) {
                root = NULL;
                lastId = -1;
            }
            else {
                if (removeItem->parent != NULL) {
                    if (data_cmp(removeItem->data, removeItem->parent->data) <= 0) {
                        if (removeItem->right != NULL) {
                            removeItem->parent->left = removeItem->right;
                        }
                        else {
                            removeItem->parent->left = removeItem->left;
                        }
                    }
                    else {
                        if (removeItem->right != NULL) {
                            removeItem->parent->right = removeItem->right;
                        }
                        else {
                            removeItem->parent->right = removeItem->left;
                        }
                    }
                }
                if (removeItem->right != NULL) { // pod��czanie do prawej dolnej ga��zi lew� ga���
                    Item* temp = removeItem->right;
                    while (temp->left != NULL) {
                        temp = temp->left;
                    }
                    temp->left = removeItem->left;
                    if (removeItem->parent == NULL) {
                        root = removeItem->right;
                        root->parent = NULL;
                    }
                }
                else if (removeItem->left != NULL) {
                    removeItem->left->parent = removeItem->parent;
                    if (removeItem->parent == NULL) {
                        root = removeItem->left;
                        root->parent = NULL;
                    }
                }
            }
            --size;
            delete(removeItem);
        }
    }

    void PreOrder() {
        return PreOrder(root);
    }
    void PreOrder(Item* item) {
        if (item != NULL) {
            std ::cout << item->data << " ";
            PreOrder(item->left);
            PreOrder(item->right);
        }
    }
    void PreOrder(Item* item, std::string& text, std::string(*dataToString)(T)) {
        if (item != NULL) {
            text += itemsToString(item, ",", dataToString);
            PreOrder(item->left, text, dataToString);
            PreOrder(item->right, text, dataToString);
        }
    }

    void InOrder() {
        return InOrder(root);
    }
    void InOrder(Item* item) {
        if (item != NULL) {
            InOrder(item->left);
            std ::cout << item->data << " ";
            InOrder(item->right);
        }
    }

    std::string ToString(std::string(*dataToString)(T)) {
        std::string finalText = "binary search tree:\n  size: " + std::to_string(size) + "\n  height: " + std::to_string(CalcHeight()) + "\n  {\n";
        PreOrder(root, finalText, dataToString);
        finalText += "  }";
        return finalText;
    }

    void Clear() {
        removeItem(root);
        root = NULL;
        size = 0;
        lastId = -1;
    }
};
/////////////////////////////////

int main()
{
    BST<int>* tree1 = new BST<int>();
    tree1->AddItem(8, compare_int);
    tree1->AddItem(3, compare_int);
    tree1->AddItem(10, compare_int);
    tree1->AddItem(1, compare_int);
    tree1->AddItem(6, compare_int);
    tree1->AddItem(4, compare_int);
    tree1->AddItem(7, compare_int);
    tree1->AddItem(14, compare_int);
    tree1->AddItem(13, compare_int);
    std::cout << "Preorder:" << std::endl;
    tree1->PreOrder();
    std::cout << std::endl << "InOrder:" << std::endl;
    tree1->InOrder();
    std::cout << std::endl << std::endl;
    std::cout << tree1->ToString(int_to_string) << std::endl;
    tree1->RemoveItem(tree1->FindItem(88, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(8, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(10, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(14, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(13, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(3, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(6, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(7, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(4, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(1, compare_int), compare_int);
    tree1->RemoveItem(tree1->FindItem(88, compare_int), compare_int);
    std::cout << std::endl << std::endl << "#AFTER REMOVE#" << std::endl;
    std::cout << tree1->ToString(int_to_string) << std::endl;
    tree1->AddItem(10, compare_int);
    tree1->AddItem(15, compare_int);
    tree1->AddItem(12, compare_int);
    tree1->AddItem(5, compare_int);
    tree1->AddItem(30, compare_int);
    tree1->AddItem(25, compare_int);
    tree1->AddItem(35, compare_int);
    tree1->AddItem(7, compare_int);
    tree1->AddItem(-2, compare_int);
    tree1->AddItem(33, compare_int);
    std::cout << std::endl << std::endl << "#BEFORE CLEAR#" << std::endl;
    std::cout << tree1->ToString(int_to_string) << std::endl;
    tree1->Clear();
    std::cout << std::endl << std::endl << "#AFTER CLEAR#" << std::endl;
    std::cout << tree1->ToString(int_to_string) << std::endl;
    BST<PersonalData2*>* tree2 = new BST<PersonalData2*>();
    PersonalData2* person1 = new PersonalData2;
    person1->surname = "asd";
    PersonalData2* person2 = new PersonalData2;
    person2->surname = "wad";
    PersonalData2* person3 = new PersonalData2;
    person3->surname = "poo";
    tree2->AddItem(person1, compare_PersonalData2);
    tree2->AddItem(person2, compare_PersonalData2);
    tree2->AddItem(person3, compare_PersonalData2);
    std::cout << tree2->ToString(PersonalData2ToString) << std::endl;
    return 0;
}
